# Wherewolf game DAO
# Abstraction for the SQL database access.

import sqlite3
import md5


class UserAlreadyExistsException(Exception):
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err
        
class NoUserExistsException(Exception):
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err
        
class BadArgumentsException(Exception):
    """Exception for entering bad arguments"""
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err

class WherewolfDao:

    def __init__(self, dbname):
        print 'Created the DAO'
        self.dbname = dbname
        self.conn = sqlite3.connect('wherewolf.db')

    def create_player(self, username, password, firstname, lastname):
        """ registers a new player in the system """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = self.conn.cursor()
            c.execute('SELECT COUNT(*) from user WHERE username=?',(username,))
            n = int(c.fetchone()[0])
            print 'num of rfdickersons is ' + str(n)
            if n == 0:
                hashedpass = md5.new(password).hexdigest()
                c.execute('INSERT INTO user (username, password, firstname, lastname) VALUES (?,?,?,?)', (username, hashedpass, firstname, lastname))
                self.conn.commit()
            else:
                raise UserAlreadyExistsException('{} user already exists'.format((username)) )
        
    def checkpassword(self, username, password):
        """ return true if password checks out """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = self.conn.cursor()
            results = c.execute('SELECT password FROM user WHERE username=?',(username,))
            hashedpass = md5.new(password).hexdigest()
            return results.fetchone()[0] == hashedpass
        
    def set_location(self, username, lat, lng):
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = self.conn.cursor()
            c.execute('UPDATE player SET lat=?,lng=? WHERE (userid=(SELECT userid FROM user WHERE username=?)) AND gameid=?',(lat, lng, username, gameid,))
            self.conn.commit()
            print('Location for {} set to ({}, {})').format(username, lat, lng)
        
    def get_location(self, username):
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = self.conn.cursor()
            lat = (c.execute('SELECT lat FROM player WHERE playerid = (SELECT playerid from player join user WHERE user.userid=player.userid and user.username=?)',(username,))).fetchone()[0]
            lng = (c.execute('SELECT lng FROM player WHERE playerid = (SELECT playerid from player join user WHERE user.userid=player.userid and user.username=?)',(username,))).fetchone()[0]
            return dict({'lat':lat, 'lng':lng})

    def get_alive_nearby(self, username):
        """ returns a list of players nearby """
        nearby_players=[]
        xcord=35
        ycord=45
        nearby=50
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = self.conn.cursor()
            is_dead = (c.execute('SELECT is_dead FROM player WHERE playerid = (SELECT playerid from player join user WHERE user.userid=player.userid and user.username=?)',(username,))).fetchone()[0]
            if is_dead==1:
                pass
            else:
                lat = (c.execute('SELECT lat FROM player WHERE playerid = (SELECT playerid from player join user WHERE user.userid=player.userid and user.username=?)',(username,))).fetchone()[0]
                lng = (c.execute('SELECT lng FROM player WHERE playerid = (SELECT playerid from player join user WHERE user.userid=player.userid and user.username=?)',(username,))).fetchone()[0]
                distance = (((lat*lat)+(lng*lng))-((xcord*xcord)+(ycord*ycord)))
                if distance<=nearby:
                    nearby_players.append(username)

    def add_item(self, username, itemname):
        """ adds a relationship to inventory and or increments quantity by 1"""
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = self.conn.cursor()
            c.execute('UPDATE inventory SET itemid=?,quantity=? WHERE (userid=(SELECT userid FROM user WHERE username =?)) AND gameid=?',(itemid, quantity, gameid,))
            self.conn.commit()
        
    def get_items(self, username):
        """ get a list of items the user has"""
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('SELECT userid from user WHERE username=?',(username,))
            userid = c.fetchone()[0]
            c.execute('SELECT playerid from player WHERE userid=?',(userid,))
            playerid = c.fetchone()[0]
            c.execute('SELECT itemid,quantity from inventory WHERE playerid=?',(playerid,))
            item_id_list = list( c.fetchall())
            
            inventory = []
            
            for item_index in item_id_list:
                itemid = item_index[0]
                quantity = item_index[1]
                
                c.execute('SELECT name, description from item WHERE itemid=?',(itemid,))
                items = c.fetchone()
                name = items[0]
                description = items[1]
                
                item = {}
                item[ 'name' ] = name
                item[ 'desciption' ] = description
                item[ 'quantity' ] = quantity

                inventory.append( item )

            return inventory
            
        
    def award_achievement(self, username, achievementname):
        """ award an achievement to the user """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('SELECT userid from user WHERE username=?',(username,))
            userid = int( c.fetchone()[0] )
            c.execute('SELECT achievementid from achievement WHERE name=?',(achievementname,))
            achievementid = int( c.fetchone()[0] )
            c.execute( 'INSERT INTO user_achievement (userid, achievementid) VALUES (?,?)',( userid, achievementid,))
            c.commit()
        
    def get_achievements(self, username):
        """ return a list of achievements for the user """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('SELECT userid from user WHERE username=?',(username,))
            userid = c.fetchone()[0]
            c.execute('SELECT achievementid from user_achievement WHERE userid=? ORDER BY created_at DESC LIMIT 10',(userid,))
            achievementid = list(c.fetchall())
            achievements = []
            
            for id_index in achievementid:

                c.execute( 'SELECT name from achievement WHERE achievementid=?',(id_index[0],))
                one = c.fetchone()[0]
                print(one)
                print(achievements)
                achievements.append(one)
            
                           
            return achievements
        
    def set_dead(self, username):
        """ set a player as dead """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('SELECT userid from user WHERE username=?',(username))
            userid = c.fetchone()[0]
            c.execute('UPDATE player SET isDead=? WHERE userid=?',(1,userid,))
            c.commit()


        #Not sure how to do this one at all, can't decide what gamename is supposed to be.
    '''def get_players(self, gamename):
        """ get information about all the players currently in the game """
        pass'''
        
    def get_user_stats(self, username):
        """ return a list of all stats for the user """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('SELECT userid from user WHERE username=?',(username))
            userid = c.fetchone()[0]
            c.execute('SELECT statName from user_stat WHERE userid=?',(userid,))
            statNames = list(c.fetchall())

            return statNames
        
    def get_player_stats(self, username):
        """ return a list of all stats for the player """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('SELECT userid from user WHERE username=?',(username))
            userid = c.fetchone()[0]
            c.execute('SELECT statName from player_stat WHERE userid=?',(userid,))
            statNames = list(c.fetchall())

            return statNames
        
    # game methods


    #Not sure about the values for the game table in SQL.
    def join_game(self, username, gameid):
        """ makes a player for a user. adds player to a game """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('INSERT INTO user VALUES (userid=?,firstname=?,lastname=?,created_at=?,username=?,password=?)',(userid, firstname, lastname, created_at, username, password))
            c.commit()
            c.execute('INSERT INTO game VALUES (username=?)',(username))
            c.commit()
    
    def leave_game(self, username):
        """ deletes player for user. removes player from game"""
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('SELECT userid from user WHERE username=?',(username))
            userid = c.fetchone()[0]
            c.execute('DELETE from player WHERE userid=?',(userid,))
            c.execute('UPDATE current_player=? WHERE userid=?',(NULL, userid,))
            c.commit()
        
    def create_game(self, username):
        """ creates a new game """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('SELECT userid from user WHERE username=?',(username))
            userid = c.fetchone()[0]
            
            c.execute('INSERT INTO game (adminid, name) VALUES (?,?)',(userid, name,))
            c.commit()

    def start_game(self, gameid):
        """ set the game as started """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('UPDATE status=? WHERE gameid=?',(1,gameid,))
            c.commit()

    def end_game(self, gameid):
        """ delete all players in the game, set game as completed """
        conn = sqlite3.connect(self.dbname)
        with conn:
            c = conn.cursor()
            c.execute('SELECT playerid from player WHERE gameid=?',(gameid,))
            playerids = list(c.fetchall())

            for player_index in playerids:
                c.execute('SELECT userid from player WHERE playerid=?', (player_index,))
                userid = c.fetchone()[0]
                c.execute('SELECT username from user WHERE userid=?',(userid,))
                username = c.fetchone()[0]
                self.leave_game( username )

            c.execute('UPDATE status=? WHERE gameid=?',(2,gameid,))
            c.commit
            
if __name__ == "__main__":
    dao = WherewolfDao('wherewolf.db')
    try:
        dao.create_player('rfdickerson', 'awesome', 'Robert', 'Dickerson')
        print 'Created a new player!'
    except UserAlreadyExistsException as e:
        print e
    except Exception:
        print 'General error happened'
        
    username = 'rfdickerson'
    correct_pass = 'awesome'
    incorrect_pass = 'scaley'
    print 'Logging in {} with {}'.format(username, correct_pass)
    print 'Result: {} '.format( dao.checkpassword(username, correct_pass ))
    
    print 'Logging in {} with {}'.format(username, incorrect_pass)
    print 'Result: {} '.format( dao.checkpassword(username, incorrect_pass ))
    
